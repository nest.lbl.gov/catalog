# `catalog_client` project #

The `catalog_client` project container both the `catalog_client` package that
provides a python interface to a Catalog server, and the `catalog-cli` command line
interface that uses that package to all command line access to the server.


## `catalog-cli` executable ##

More details about the `catalog-cli` executable can be found using its help option

    catalog-cli -h
