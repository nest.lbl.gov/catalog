package gov.lbl.nest.catalog;

/**
 * This Exception is thrown when a Spade operation has failed and so its
 * 'problem' files have been saved.
 * 
 * @author patton
 */
public class ResolutionException extends
                                 Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param cause
     *            the Exception that caused this object was thrown.
     */
    public ResolutionException(final Throwable cause) {
        super(cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
