package gov.lbl.nest.catalog;

import java.net.URL;

import gov.lbl.nest.catalog.mongo.EntryDocument;
import gov.lbl.nest.catalog.rs.EntryImpl;
import gov.lbl.nest.catalog.rs.Tags;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlTransient;

/**
 * This interface provides access to a single entry in the catalog.
 * 
 * @author patton
 */
@XmlSeeAlso(value = { EntryImpl.class,
                      EntryDocument.class })
public abstract class Entry {

    /**
     * Returns the identity of the entity that this object encapsulates.
     * 
     * @return the identity of the entity that this object encapsulates.
     */
    public abstract String getIdentity();

    /**
     * Returns the tags associated with this object.
     * 
     * @return the tags associated with this object.
     */
    public abstract Tags getTags();

    /**
     * Returns the URL where the entity this object encapsulates.
     * 
     * @return the URL of the entity this object encapsulates.
     */
    @XmlTransient
    public abstract URL getURL();

    /**
     * Sets the URL where the entity encapsulated by this object can be found.
     * 
     * @param url
     *            the URL where the entity encapsulated by this object can be found.
     */
    public abstract void setURL(final URL url);

}