package gov.lbl.nest.catalog;

import java.net.URL;
import java.util.List;

/**
 * This interface is used to resolve the URL of an entry using its identity.
 * 
 * @author patton
 *
 */
public interface URLResolver {

    /**
     * Return and list of URLs matching, one for one, the list of supplied
     * identities.
     * 
     * @param identities
     *            the identities whose URLs should be returned.
     * 
     * @return the {@link List} of URLs
     * 
     * @throws ResolutionException
     *             when the URL can not be resolved correctly.
     */
    List<URL> getURLs(List<String> identities) throws ResolutionException;

}
