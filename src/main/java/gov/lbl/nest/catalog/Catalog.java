package gov.lbl.nest.catalog;

import java.util.List;

import gov.lbl.nest.catalog.rs.Entries;
import gov.lbl.nest.catalog.rs.EntryImpl;

/**
 * This interface defines the methods that a {@link Catalog} application needs
 * in implement.
 * 
 * @author patton
 * 
 */
public interface Catalog {

    /**
     * The version of the specification that this library implements.
     */
    static final String SPECIFICATION = "2.0";

    /**
     * Adds the specified {@link Entry} instance to the catalog. If the supplied
     * {@link Entry} exists, this method will fail and the {@link #update(Entry)}
     * method should be invoked.
     * 
     * @param entry
     *            the {@link Entry} instance to be added.
     * 
     * @return true if the {@link Entry} has been successfully added.
     */
    boolean add(final Entry entry);

    /**
     * Returns the list of all {@link EntryImpl} instance in the catalog that match
     * that meet specified {@link Criterium}.
     * 
     * @param criteria
     *            the {@link Criterium} that all returned {@link EntryImpl}
     *            instances must match.
     * @param alltags
     *            true is all tags should be included in which case the contents of
     *            the tags parameter will be ignored.
     * @param tags
     *            the list of tags to include in the result.
     * @param ordering
     *            the sequence of tags that should be used for ordering the
     *            resulting list. (The tags enumerated here are not required to be
     *            present in the <code>tags</code> parameter.
     * 
     * @return the {@link Entries} instance of all {@link EntryImpl} instance in the
     *         catalog that match that meet specified criteria.
     * 
     * @throws ResolutionException
     *             when the URL of an item can not be resolved correctly.
     */
    Entries get(final Criterium criteria,
                boolean alltags,
                List<String> tags,
                List<String> ordering) throws ResolutionException;

    /**
     * Updates the specified {@link Entry} instance to the catalog. If the supplied
     * {@link Entry} does not exist, this method will fail and the
     * {@link #add(Entry)} method should be invoked.
     * 
     * @param entry
     *            the {@link Entry} instance to be updated.
     * 
     * @return the updated Entry if the {@link Entry} has been successfully updated,
     *         or <code>null</code> otherwise.
     */
    Entry update(final Entry entry);

}
