package gov.lbl.nest.catalog.execute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.catalog.CatalogDB;
import gov.lbl.nest.catalog.ImplementationString;
import gov.lbl.nest.catalog.mongo.DocumentManager;
import gov.lbl.nest.common.execution.DeployedFiles;
import gov.lbl.nest.common.execution.DeployedFiles.DeployedFile;
import jakarta.ejb.Singleton;
import jakarta.enterprise.inject.Produces;

/**
 * This class provides resources to be injected into the SPADE application.
 * 
 * @author patton
 */
@Singleton
public class ResourcesForCatalog {

    /**
     * Returns the version String containing this project's release.
     * 
     * @return the version String containing this project's release.
     */
    private static synchronized String getDeployedVersion() {
        if (null == version) {
            DeployedFile deployedFile = DeployedFiles.getDeployedWarFile();
            LOG.info("Using version \"" + deployedFile.version
                     + "\" of artifact \""
                     + deployedFile.project
                     + "\"");
            version = deployedFile.version;
        }
        return version;
    }

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ResourcesForCatalog.class);

    // private static member data

    /**
     * Expose an entity manager using the resource producer pattern
     */
    @Produces
    @CatalogDB
    private static DocumentManager documentManager = new DocumentManager();

    /**
     * The version String containing this project's release.
     */
    private static String version;

    /**
     * Creates an instance of this class.
     */
    public ResourcesForCatalog() {
        getDeployedVersion();
    }

    /**
     * Returns the implementation version.
     * 
     * @return the implementation version.
     */
    @Produces
    @ImplementationString
    public String getImplementationString() {
        getDeployedVersion();
        return version;
    }
}
