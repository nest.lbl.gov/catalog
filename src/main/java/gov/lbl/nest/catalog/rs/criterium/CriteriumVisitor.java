package gov.lbl.nest.catalog.rs.criterium;

import gov.lbl.nest.catalog.Criterium;

/**
 * This interface is used to build a compound {@link Criterium} from collection
 * of {@link Criterium} instances.
 * 
 * @author patton
 *
 */
public interface CriteriumVisitor {

    /**
     * Visits a {@link Equality} instance to build this objects criteria.
     * 
     * @param criterium
     *            the {@link Equality} instance to visit.
     */
    void visit(final Equality criterium);

    /**
     * Visits a {@link Greater} instance to build this objects criteria.
     * 
     * @param criterium
     *            the {@link Equality} instance to visit.
     */
    void visit(final Greater criterium);

    /**
     * Visits a {@link GreaterOrEqual} instance to build this objects criteria.
     * 
     * @param criterium
     *            the {@link Equality} instance to visit.
     */
    void visit(final GreaterOrEqual criterium);

    /**
     * Visits a {@link Less} instance to build this objects criteria.
     * 
     * @param criterium
     *            the {@link Equality} instance to visit.
     */
    void visit(final Less criterium);

    /**
     * Visits a {@link LessOrEqual} instance to build this objects criteria.
     * 
     * @param criterium
     *            the {@link Equality} instance to visit.
     */
    void visit(final LessOrEqual criterium);

    /**
     * Visits a {@link LogicalAnd} instance to build this objects criteria.
     * 
     * @param criterium
     *            the {@link LogicalAnd} instance to visit.
     */
    void visit(final LogicalAnd criterium);

    /**
     * Visits a {@link LogicalNot} instance to build this objects criteria.
     * 
     * @param criterium
     *            the {@link LogicalNot} instance to visit.
     */
    void visit(final LogicalNot criterium);

    /**
     * Visits a {@link LogicalOr} instance to build this objects criteria.
     * 
     * @param criterium
     *            the {@link LogicalOr} instance to visit.
     */
    void visit(final LogicalOr criterium);
}
