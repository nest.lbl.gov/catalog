package gov.lbl.nest.catalog.rs;

import java.net.URL;
import java.util.List;

import gov.lbl.nest.catalog.Entry;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is the RESTful encapsulation of an entry in the Catalog.
 * 
 * @author patton
 */
@XmlRootElement(name = "entry")
@XmlType(propOrder = { "identity",
                       "URL",
                       "tags" })
public class EntryImpl extends
                       Entry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The sequence, if any, of {@link Object} instances that implement the
     * {@link Comparable} interface that are used to order the {@link Entry}
     * instances.
     */
    private List<? extends Comparable<?>> comparables;

    /**
     * The identity of the entity that this object encapsulates.
     */
    private String identity;

    /**
     * The tags associated with this object.
     */
    private Tags tags;

    /**
     * The URL where the entity encapsulated by this object can be found.
     */
    private URL url;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected EntryImpl() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param identity
     *            the identity of the entity that this object encapsulates.
     * @param url
     *            the URL where the entity encapsulated by this object can be found.
     * @param tags
     *            the tags associated with this object.
     * @param comparables
     *            the sequence, if any, of {@link Object} instances that implement
     *            the {@link Comparable} interface that are used to order the
     *            {@link Entry} instances.
     */
    protected EntryImpl(final String identity,
                        final URL url,
                        final Tags tags,
                        List<? extends Comparable<?>> comparables) {
        this.comparables = comparables;
        setIdentity(identity);
        setTags(tags);
        setURL(url);
    }

    // instance member method (alphabetic)

    /**
     * Returns the sequence, if any, of {@link Object} instances that implement the
     * {@link Comparable} interface that are used to order the {@link Entry}
     * instances.
     * 
     * @return the sequence, if any, of {@link Object} instances that implement the
     *         {@link Comparable} interface that are used to order the {@link Entry}
     *         instances.
     */
    @XmlTransient
    public List<? extends Comparable<?>> getComparables() {
        return comparables;
    }

    @Override
    @XmlElement
    public String getIdentity() {
        return identity;
    }

    @Override
    @XmlElement
    public Tags getTags() {
        return tags;
    }

    @Override
    @XmlElement(name = "url")
    public URL getURL() {
        return url;
    }

    /**
     * Sets the identity of the entity that this object encapsulates.
     * 
     * @param identity
     *            the identity of the entity that this object encapsulates.
     */
    protected void setIdentity(final String identity) {
        this.identity = identity;
    }

    /**
     * Sets the tags associated with this object.
     * 
     * @param tags
     *            the tags associated with this object.
     */
    protected void setTags(final Tags tags) {
        this.tags = tags;
    }

    @Override
    public void setURL(final URL url) {
        this.url = url;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
