package gov.lbl.nest.catalog.rs.criterium;

import java.util.List;

import gov.lbl.nest.catalog.Criterium;
import gov.lbl.nest.catalog.Entry;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * This class implements the {@link Criterium} interface such that it matches
 * all {@link Entry} instances fail to match a particular {@link Criterium}
 * instances.
 * 
 * @author patton
 */
@XmlRootElement(name = "not")
public class LogicalNot extends
                        CriteriaArray {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public void build(CriteriumVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Returns the single {@link Criterium} instance contained in this object.
     * 
     * @return the single {@link Criterium} instance contained in this object.
     * 
     * @throws IllegalStateException
     */
    protected Criterium getCriterium() throws IllegalStateException {
        final List<Criterium> criteria = getCriteria();
        if (1 != criteria.size()) {
            throw new IllegalStateException("A \"not\" selection must container one and only one cirterium");
        }
        return getCriteria().get(0);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
