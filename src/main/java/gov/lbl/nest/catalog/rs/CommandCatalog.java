package gov.lbl.nest.catalog.rs;

import gov.lbl.nest.catalog.Catalog;
import gov.lbl.nest.catalog.Entry;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.UriInfo;

/**
 * The class provides the write access RESTful interface to access the Catalog
 * application commands.
 * 
 * @author patton
 * 
 * @see CommandCatalog
 */
@Path("report")
@Stateless
public class CommandCatalog {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Catalog} instance used by this instance.
     */
    @Inject
    private Catalog catalog;

    // constructors

    // instance member method (alphabetic)

    /**
     * Adds this supplied {@link EntryImpl} to the catalog.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param entry
     *            the {@link EntryImpl} instance to be added.
     * 
     * @return the representation of this application.
     */
    @POST
    @Path("add")
    @Consumes({ CatalogType.ENTRIES_XML,
                CatalogType.ENTRIES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response addEntry(@Context UriInfo uriInfo,
                             Entry entry) {
        ResponseBuilder builder;
        try {
            if (catalog.add(entry)) {
                builder = Response.ok();
            } else {
                builder = Response.status(Status.BAD_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
