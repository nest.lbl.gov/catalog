package gov.lbl.nest.catalog.rs;

/**
 * The class contains the media types used by the RESTful interface.
 * 
 * @author patton
 * 
 */
public class CatalogType {

    /**
     * The media type for the XML representation of an {@link Application} instance.
     */
    public static final String APPLICATION_XML = "application/gov.lbl.nest.catalog.rs.Application+xml";

    /**
     * The media type for the JSON representation of an {@link Application}
     * instance.
     */
    public static final String APPLICATION_JSON = "application/gov.lbl.nest.catalog.rs.Application+json";

    /**
     * The media type for the XML representation of an {@link Entries} instance.
     */
    public static final String ENTRIES_XML = "application/gov.lbl.nest.catalog.rs.Entries+xml";

    /**
     * The media type for the JSON representation of an {@link Entries} instance.
     */
    public static final String ENTRIES_JSON = "application/gov.lbl.nest.catalog.rs.Entries+json";

}
