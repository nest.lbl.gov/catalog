package gov.lbl.nest.catalog.rs;

import java.net.URI;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class represents a user's entry point to all accessible resources in the
 * application.
 * 
 * @author patton
 */
@XmlRootElement(name = "application")
@XmlType(propOrder = { "uri",
                       "actions",
                       "specification",
                       "implementation" })
public class Application {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The group of actions that this application can execute.
     */
    private ActionGroup actions;

    /**
     * The implementation version of this application.
     */
    private String implementation;

    /**
     * The specification version this application implements.
     */
    private String specification;

    /**
     * The URI of this object.
     */
    private URI uri;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Application() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param uri
     *            the URI of this object.
     * @param actions
     *            TODO
     * @param specification
     *            the specification version this application implements.
     * @param implementation
     *            the implementation version of this application.
     */
    Application(final URI uri,
                final ActionGroup actions,
                final String specification,
                String implementation) {
        setActions(actions);
        setImplementation(implementation);
        setSpecification(specification);
        setUri(uri);
    }

    // instance member method (alphabetic)

    /**
     * Returns the group of actions that this application can execute.
     * 
     * @return the group of actions that this application can execute.
     */
    @XmlElement
    protected ActionGroup getActions() {
        return actions;
    }

    /**
     * Returns the implementation version of this application.
     * 
     * @return the implementation version of this application.
     */
    @XmlElement
    protected String getImplementation() {
        return implementation;
    }

    /**
     * Returns the specification version this application implements.
     * 
     * @return the specification version this application implements.
     */
    @XmlElement
    protected String getSpecification() {
        return specification;
    }

    /**
     * Returns the URI of this object.
     * 
     * @return the URI of this object.
     */
    @XmlElement(name = "uri")
    protected URI getUri() {
        return uri;
    }

    /**
     * Sets the group of actions that this application can execute.
     * 
     * @param actions
     *            the group of actions that this application can execute.
     */
    protected void setActions(ActionGroup actions) {
        this.actions = actions;
    }

    /**
     * Sets the implementation version of this application.
     * 
     * @param implementation
     *            the implementation version of this application.
     */
    protected void setImplementation(String implementation) {
        this.implementation = implementation;
    }

    /**
     * Sets the specification version this application implements.
     * 
     * @param specification
     *            the specification version this application implements.
     */
    protected void setSpecification(String specification) {
        this.specification = specification;
    }

    /**
     * Sets the URI of this object.
     * 
     * @param uri
     *            the URI of this object.
     */
    protected void setUri(URI uri) {
        this.uri = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
