package gov.lbl.nest.catalog.rs.criterium;

import gov.lbl.nest.catalog.Criterium;
import gov.lbl.nest.catalog.Entry;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * This class implements the {@link Criterium} interface such that it matches
 * all {@link Entry} instances that match at least one of a set of
 * {@link Criterium} instances.
 * 
 * @author patton
 */
@XmlRootElement(name = "or")
public class LogicalOr extends
                       CriteriaArray {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public void build(CriteriumVisitor visitor) {
        visitor.visit(this);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
