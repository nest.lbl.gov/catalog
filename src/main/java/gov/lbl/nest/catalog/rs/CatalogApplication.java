package gov.lbl.nest.catalog.rs;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

/**
 * This class provides the location of the RESTful access to the PSquared
 * application.
 * 
 * @author patton
 */
@ApplicationPath("local")
public class CatalogApplication extends
                                Application {
    // Does nothing, just a label.
}
