package gov.lbl.nest.catalog.rs.criterium;

import gov.lbl.nest.catalog.Criterium;
import gov.lbl.nest.catalog.Entry;
import gov.lbl.nest.common.xml.item.Value;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class implements the {@link Criterium} interface such that it matches
 * all {@link Entry} instances that have a field which is greater than or equal
 * to a particular value.
 * 
 * @author patton
 */
@XmlRootElement(name = "gte")
@XmlType(propOrder = { "name",
                       "itemValue" })
public class GreaterOrEqual extends
                            Criterium {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The name of this item in the Workflow Definition.
     */
    private String name;

    /**
     * The XML representation of the value of this item.
     */
    private Value value;

    // constructors

    /**
     * Creates an instance of this object.
     */
    protected GreaterOrEqual() {
    }

    /**
     * Creates an instance of this object.
     * 
     * @param name
     *            the name of this item in the Workflow Definition.
     * @param value
     *            the value of this object.
     */
    public GreaterOrEqual(String name,
                          Object value) {
        setName(name);
        setValue(value);
    }

    // instance member method (alphabetic)

    @Override
    public void build(CriteriumVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Returns the XML representation of the value of this item.
     * 
     * @return the XML representation of the value of this item.
     */
    @XmlElement(name = "value")
    public Value getItemValue() {
        return value;
    }

    /**
     * Returns the name of this item in the Workflow Definition.
     * 
     * @return the name of this item in the Workflow Definition.
     */
    @XmlElement
    public String getName() {
        return name;
    }

    /**
     * Returns the value of this item within a Workflow instance.
     * 
     * @return the value of this item within a Workflow instance.
     */
    @XmlTransient
    public Object getValue() {
        if (null == value) {
            return null;
        }
        return value.getObject();
    }

    /**
     * Sets the XML representation of the value of this item.
     * 
     * @param value
     *            the XML representation of the value of this item.
     */
    public void setItemValue(Value value) {
        this.value = value;
    }

    /**
     * Sets the name of this item in the Workflow Definition.
     * 
     * @param name
     *            the name of this item in the Workflow Definition.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Set the value of this item within a Workflow instance.
     * 
     * @param value
     *            the value of this item within a Workflow instance.
     */
    public void setValue(Object value) {
        final Value itemValue = new Value(value);
        setItemValue(itemValue);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
