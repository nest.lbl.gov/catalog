package gov.lbl.nest.catalog.rs;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is the RESTful encapsulation of a numeric range.
 * 
 * @author patton
 */
@XmlType(propOrder = { "begin",
                       "end",
                       "step" })
public class IntegerRange {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The Integer that is at the beginning of the range.
     */
    private Integer begin;

    /**
     * The Integer that the range should not reach.
     */
    private Integer end;

    /**
     * The size of the steps to take when resolving the range of Integers.
     */
    private Integer step;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the Integer that is at the beginning of the range.
     * 
     * @return the Integer that is at the beginning of the range.
     */
    @XmlElement
    public Integer getBegin() {
        return begin;
    }

    /**
     * Returns the Integer that the range should not reach.
     * 
     * @return the Integer that the range should not reach.
     */
    @XmlElement
    public Integer getEnd() {
        return end;
    }

    /**
     * Returns the size of the steps to take when resolving the range of Integers.
     * 
     * @return the size of the steps to take when resolving the range of Integers.
     */
    @XmlElement
    public Integer getStep() {
        return step;
    }

    /**
     * Sets the Integer that is at the beginning of the range.
     * 
     * @param begin
     *            the Integer that is at the beginning of the range.
     */
    protected void setBegin(Integer begin) {
        this.begin = begin;
    }

    /**
     * Sets the Integer that the range should not reach.
     * 
     * @param end
     *            the Integer that the range should not reach.
     */
    protected void setEnd(Integer end) {
        this.end = end;
    }

    /**
     * Sets the size of the steps to take when resolving the range of Integers.
     * 
     * @param step
     *            the size of the steps to take when resolving the range of
     *            Integers.
     */
    protected void setStep(Integer step) {
        this.step = step;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
