package gov.lbl.nest.catalog.rs.criterium;

import java.util.List;

import gov.lbl.nest.catalog.Criterium;
import jakarta.xml.bind.annotation.XmlAnyElement;

/**
 * This class is the base class of all {@link Criterium} instances that operate
 * on an array of {@link Criterium}.
 * 
 * @author patton
 */
public abstract class CriteriaArray extends
                                    Criterium {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The set of {@link Criterium} instances to combine logically.
     */
    private List<Criterium> criteria;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the set of {@link Criterium} instances to combine logically.
     * 
     * @return the set of {@link Criterium} instances to combine logically.
     */
    @XmlAnyElement(lax = true)
    protected List<Criterium> getCriteria() {
        return criteria;
    }

    /**
     * Sets the set of {@link Criterium} instances to combine logically.
     * 
     * @param criteria
     *            the set of {@link Criterium} instances to combine logically.
     */
    protected void setCriteria(List<Criterium> criteria) {
        this.criteria = criteria;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
