package gov.lbl.nest.catalog.rs;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.catalog.Catalog;
import gov.lbl.nest.catalog.Criterium;
import gov.lbl.nest.catalog.ImplementationString;
import gov.lbl.nest.common.rs.NamedResource;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

/**
 * The class provides the read-only RESTful interface to access the Catalog
 * application status.
 * 
 * @author patton
 * 
 * @see CommandCatalog
 */
@Path("report")
@Stateless
public class ReportCatalog {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ReportCatalog.class);

    // private static member data

    // private instance member data

    /**
     * The {@link Catalog} instance used by this instance.
     */
    @Inject
    private Catalog catalog;

    /**
     * The implementation version.
     */
    @Inject
    @ImplementationString
    private String implementation;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the list of all {@link EntryImpl} instance in the catalog that match
     * that meet specified {@link Criterium}.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param criteria
     *            the {@link Criterium} that all returned {@link EntryImpl}
     *            instances must match.
     * @param alltags
     *            true is all tags should be included in which case the contents of
     *            the tags parameter will be ignored.
     * @param tags
     *            the list of tags to include in the result.
     * @param ordering
     *            the sequence of tags that should be used for ordering the
     *            resulting list. (The tags enumerated here are not required to be
     *            present in the <code>tags</code> parameter.
     * 
     * @return the {@link Entries} instance of all {@link EntryImpl} instance in the
     *         catalog that match that meet specified criteria.
     */
    @GET
    @Path("get")
    @Produces({ CatalogType.ENTRIES_XML,
                CatalogType.ENTRIES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response get(@Context UriInfo uriInfo,
                        final Criterium criteria,
                        @QueryParam("alltags") Boolean alltags,
                        @QueryParam("tag") List<String> tags,
                        @QueryParam("ordering") List<String> ordering) {
        LOG.debug("Query parameter alltags is:" + alltags);
        LOG.debug("Query parameter tags is:" + tags);
        LOG.debug("Query parameter ordering is:" + ordering);
        ResponseBuilder builder;
        try {
            builder = Response.ok(catalog.get(criteria,
                                              null != alltags && alltags,
                                              tags,
                                              ordering));
        } catch (Exception e) {
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Returns the representation of this application.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * 
     * @return the representation of this application.
     */
    @GET
    @Path("")
    @Produces({ CatalogType.APPLICATION_XML,
                CatalogType.APPLICATION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Application getApplication(@Context UriInfo uriInfo) {
        final URI baseUri = uriInfo.getBaseUri();
        final URI reportUri = baseUri.resolve("report/");

        final ActionGroup actions = new ActionGroup();
        final List<NamedResource> applicationActions = new ArrayList<NamedResource>();
        actions.setActions(applicationActions);
        applicationActions.add(new NamedResource(reportUri.resolve("get"),
                                                 "selection",
                                                 "Selects entries from the catalog based on criteria"));

        return new Application(reportUri,
                               actions,
                               Catalog.SPECIFICATION,
                               implementation);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
