package gov.lbl.nest.catalog.rs.criterium;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;

import com.mongodb.client.model.Filters;

import gov.lbl.nest.catalog.Criterium;

/**
 * This class is used to build a compound criterium for a tree of criteria.
 * 
 * @author patton
 */
public class MongoCriteriumVisitor implements
                                   CriteriumVisitor {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The LIFO list of {@link Bson} instances being used to create the compound
     * criterium.
     */
    private List<Bson> criteriumLifo = new ArrayList<Bson>();

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the current representation of the compound criterium being created.
     * 
     * @return the current representation of the compound criterium being created.
     */
    public Bson getCompoundCriterium() {
        if (1 != criteriumLifo.size()) {
            throw new IllegalStateException();
        }
        return criteriumLifo.get(0);
    }

    @Override
    public void visit(Equality criterium) {
        final Bson filter = Filters.eq(criterium.getName(),
                                       criterium.getValue());
        criteriumLifo.add(filter);
    }

    @Override
    public void visit(Greater criterium) {
        final Bson filter = Filters.gt(criterium.getName(),
                                       criterium.getValue());
        criteriumLifo.add(filter);
    }

    @Override
    public void visit(GreaterOrEqual criterium) {
        final Bson filter = Filters.gte(criterium.getName(),
                                        criterium.getValue());
        criteriumLifo.add(filter);
    }

    @Override
    public void visit(Less criterium) {
        final Bson filter = Filters.lt(criterium.getName(),
                                       criterium.getValue());
        criteriumLifo.add(filter);
    }

    @Override
    public void visit(LessOrEqual criterium) {
        final Bson filter = Filters.lte(criterium.getName(),
                                        criterium.getValue());
        criteriumLifo.add(filter);
    }

    @Override
    public void visit(LogicalAnd criterium) {
        final List<Criterium> criteria = criterium.getCriteria();
        final List<Bson> bsons = new ArrayList<Bson>();
        for (Criterium item : criteria) {
            item.build(this);
            bsons.add(criteriumLifo.remove(criteriumLifo.size() - 1));
        }
        criteriumLifo.add(Filters.and(bsons));
    }

    @Override
    public void visit(LogicalNot criterium) {
        try {
            final Criterium item = criterium.getCriterium();
            item.build(this);
            criteriumLifo.add(Filters.not(criteriumLifo.remove(criteriumLifo.size() - 1)));
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void visit(LogicalOr criterium) {
        final List<Criterium> criteria = criterium.getCriteria();
        final List<Bson> bsons = new ArrayList<Bson>();
        for (Criterium item : criteria) {
            item.build(this);
            bsons.add(criteriumLifo.remove(criteriumLifo.size() - 1));
        }
        criteriumLifo.add(Filters.or(bsons));
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
