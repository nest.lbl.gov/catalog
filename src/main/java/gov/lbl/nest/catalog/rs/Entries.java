package gov.lbl.nest.catalog.rs;

import java.util.List;

import gov.lbl.nest.catalog.Entry;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to group a collection of {@link Entry} instances into a
 * single object. (This helps to support JSON parsing, which does not appear to
 * handle XmlElementWrapper annotations correctly!)
 * 
 * @author patton
 *
 */
@XmlRootElement(name = "entries")
@XmlType(propOrder = { "content" })
public class Entries {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The Collection of {@link Entry} instances in this object.
     */
    private List<? extends Entry> content;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Entries() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param content
     *            the Collection of {@link Entry} instances in this object.
     */
    public Entries(List<? extends Entry> content) {
        setContent(content);
    }

    // instance member method (alphabetic)

    /**
     * Returns the Collection of {@link Entry} instances in this object.
     * 
     * @return the Collection of {@link Entry} instances in this object.
     */
    @XmlElement(name = "entry")
    public List<? extends Entry> getContent() {
        return content;
    }

    /**
     * Sets the Collection of {@link Entry} instances in this object.
     * 
     * @param content
     *            the Collection of {@link Entry} instances in this object.
     */
    public void setContent(List<? extends Entry> content) {
        this.content = content;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
