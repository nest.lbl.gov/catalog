package gov.lbl.nest.catalog.spade;

import java.util.Date;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class represents the location of one or more files, related to a Bundle,
 * in the Warehouse.
 * 
 * @author patton
 */
@XmlRootElement
@XmlType(propOrder = { "identity",
                       "whenLastModified",
                       "metadata",
                       "data",
                       "wrapped",
                       "compressed" })
public class Location {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The path to the compressed file of this object.
     */
    private String compressed;

    /**
     * The path to the data file of this object.
     */
    private String data;

    /**
     * The unique identity of this object within the warehouse.
     */
    private String identity;

    /**
     * The path to the metadata file of this object.
     */
    private String metadata;

    /**
     * The date and time this object in the warehouse was modified.
     */
    private Date whenLastModified;

    /**
     * The path to the wrapped file of this object.
     */
    private String wrapped;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Location() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param identity
     *            the unique identity of this object within the warehouse.
     * @param data
     *            the path to the data file of this object.
     */
    public Location(final String identity,
                    final String data) {
        this(identity,
             null,
             data,
             null,
             null,
             null);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param metadata
     *            the path to the metadata file of this object.
     * @param data
     *            the path to the data file of this object.
     * @param wrapped
     *            the path to the wrapped file of this object.
     * @param compressed
     *            the path to the compressed file of this object.
     */
    public Location(final String metadata,
                    final String data,
                    final String wrapped,
                    final String compressed) {
        this(null,
             metadata,
             data,
             wrapped,
             compressed,
             null);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param identity
     *            the unique identity of this object within the warehouse.
     * @param metadata
     *            the path to the metadata file of this object.
     * @param data
     *            the path to the data file of this object.
     * @param wrapped
     *            the path to the wrapped file of this object.
     * @param compressed
     *            the path to the compressed file of this object.
     * @param dateTime
     *            the date and time this object in the warehouse was modified.
     */
    public Location(final String identity,
                    final String metadata,
                    final String data,
                    final String wrapped,
                    final String compressed,
                    final Date dateTime) {
        setCompressed(compressed);
        setData(data);
        setIdentity(identity);
        setMetadata(metadata);
        setWhenLastModified(dateTime);
        setWrapped(wrapped);
    }

    // instance member method (alphabetic)

    /**
     * Returns the path to the compressed file of this object.
     * 
     * @return the path to the compressed file of this object.
     */
    @XmlElement
    public String getCompressed() {
        return compressed;
    }

    /**
     * Returns the path to the data file of this object.
     * 
     * @return the path to the data file of this object.
     */
    @XmlElement
    public String getData() {
        return data;
    }

    /**
     * Returns the unique identity of this object within the warehouse.
     * 
     * @return the unique identity of this object within the warehouse.
     */
    @XmlElement
    public String getIdentity() {
        return identity;
    }

    /**
     * Returns the path to the metadata file of this object.
     * 
     * @return the path to the metadata file of this object.
     */
    @XmlElement
    public String getMetadata() {
        return metadata;
    }

    /**
     * Returns the date and time this object in the warehouse was modified.
     * 
     * @return the date and time this object in the warehouse was modified.
     */
    @XmlElement(name = "lastModified")
    public Date getWhenLastModified() {
        return whenLastModified;
    }

    /**
     * Returns the path to the wrapped file of this object.
     * 
     * @return the path to the wrapped file of this object.
     */
    @XmlElement
    public String getWrapped() {
        return wrapped;
    }

    /**
     * Sets the path to the compressed file of this object.
     * 
     * @param compressed
     *            the path to the compressed file of this object.
     */
    protected void setCompressed(String compressed) {
        this.compressed = compressed;
    }

    /**
     * Sets the path to the data file of this object.
     * 
     * @param path
     *            the path to the data file of this object.
     */
    protected void setData(final String path) {
        data = path;
    }

    /**
     * Sets the unique identity of this object within the warehouse.
     * 
     * @param identity
     *            the unique identity of this object within the warehouse.
     */
    protected void setIdentity(final String identity) {
        this.identity = identity;
    }

    /**
     * Sets the path to the metadata file of this object.
     * 
     * @param path
     *            the path to the metadata file of this object.
     */
    protected void setMetadata(final String path) {
        metadata = path;
    }

    /**
     * Sets the date and time this object in the warehouse was modified.
     * 
     * @param dateTime
     *            the date and time this object in the warehouse was modified.
     */
    protected void setWhenLastModified(final Date dateTime) {
        whenLastModified = dateTime;
    }

    /**
     * Sets the path to the wrapped file of this object.
     * 
     * @param path
     *            the path to the wrapped file of this object.
     */
    protected void setWrapped(String path) {
        wrapped = path;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
