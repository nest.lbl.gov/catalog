package gov.lbl.nest.catalog.spade;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, a group of one
 * or more files the SPADE will treat as a single unit.
 * 
 * @author patton
 */
@XmlType(propOrder = { "name" })
public class Bundle {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The name of this bundle.
     */
    private String name;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Bundle() {
    }

    /**
     * Creates an instance of this class
     * 
     * @param name
     *            the name of this bundle.
     */
    public Bundle(String name) {
        setName(name);
    }

    // instance member method (alphabetic)

    /**
     * Returns the name of this bundle.
     * 
     * @return The name of this bundle.
     */
    @XmlElement
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this bundle.
     * 
     * @param name
     *            the name of this bundle.
     */
    protected void setName(final String name) {
        this.name = name;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
