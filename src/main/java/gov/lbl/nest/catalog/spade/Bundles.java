package gov.lbl.nest.catalog.spade;

import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, a group zero,
 * one or more {@link Bundle} instances.
 * 
 * @author patton
 */
@XmlRootElement(name = "bundles")
@XmlType(propOrder = { "uri",
                       "bundles" })
public class Bundles {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The collection of the {@link Bundle} instances constitute this object.
     */
    private Collection<Bundle> bundles;

    /**
     * The URI, if any, that is related to the content of this object.
     */
    private URI uri;

    // constructors

    // instance member method (alphabetic)

    /**
     * Create an instance of this class.
     */
    protected Bundles() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param url
     *            the URI, if any, that is related to the content of this object.
     * @param bundles
     *            the collection of the {@link Bundle} instances constitute this
     *            object.
     */
    public Bundles(URI url,
                   Collection<String> bundles) {
        final List<Bundle> createdBundles = new ArrayList<Bundle>();
        for (String bundle : bundles) {
            createdBundles.add(new Bundle(bundle));
        }
        setBundles(createdBundles);
        setUri(url);
    }

    /**
     * Returns the collection of the {@link Bundle} instances constitute this
     * object.
     * 
     * @return the collection of the {@link Bundle} instances constitute this
     *         object.
     */
    @XmlElement(name = "bundle")
    public Collection<Bundle> getBundles() {
        return bundles;
    }

    /**
     * Returns the URI, if any, that is related to the content of this object.
     * 
     * @return the URI, if any, that is related to the content of this object.
     */
    @XmlElement
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the collection of the {@link Bundle} instances constitute this object.
     * 
     * @param bundles
     *            the collection of the {@link Bundle} instances constitute this
     *            object.
     */
    protected void setBundles(Collection<Bundle> bundles) {
        this.bundles = bundles;
    }

    /**
     * Sets the URI, if any, that is related to the content of this object.
     * 
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     */
    protected void setUri(URI uri) {
        this.uri = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    @Override
    public String toString() {
        JAXBContext content;
        try {
            content = JAXBContext.newInstance(Bundles.class);
            final Marshaller marshaller = content.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                                   true);
            final StringWriter writer = new StringWriter();
            marshaller.marshal(this,
                               writer);
            return writer.toString();
        } catch (JAXBException e) {
            return super.toString();
        }
    }

    // public static void main(String args[]) {}
}
