package gov.lbl.nest.catalog.spade;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.catalog.ResolutionException;
import gov.lbl.nest.catalog.URLResolver;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

/**
 * This class implements the {@link URLResolver} interface top query teh
 * Lux-Zeplin SPADE instance to resolve an identities URL.
 * 
 * @author patton
 */
public class URLResolverImpl implements
                             URLResolver {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The partial URL used to resolved file locations.
     */
    private final static String PLACEMENT_URL = "bundles/placement";

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public List<URL> getURLs(List<String> identities) throws ResolutionException {
        final List<URL> result = new ArrayList<URL>();
        if (null == identities) {
            return null;
        } else if (identities.isEmpty()) {
            return result;
        }
        final Bundles bundles = new Bundles(null,
                                            identities);
        try {
            final URL url = new URL("https://lux-zeplin.lbl.gov/spade/nersc/report/" + PLACEMENT_URL);
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type",
                                          "application/gov.lbl.nest.spade.rs.Bundles+xml");
            connection.setRequestProperty("Accept",
                                          "application/gov.lbl.nest.spade.rs.Locations+xml");
            final Class<?>[] classes = new Class<?>[] { Bundles.class,
                                                        Locations.class };
            JAXBContext jc = JAXBContext.newInstance(classes);
            final Marshaller marshaller = jc.createMarshaller();
            marshaller.marshal(bundles,
                               connection.getOutputStream());
            connection.getOutputStream()
                      .close();
            if (200 != connection.getResponseCode()) {
                throw new IOException("HTTP response was " + connection.getResponseCode()
                                      + ", not 200 that is the expected value");
            }

            final InputStream is = connection.getInputStream();
            final Unmarshaller unmarshaller = jc.createUnmarshaller();
            final Locations locations = (Locations) unmarshaller.unmarshal(is);
            final List<Location> content = locations.getContent();
            for (Location location : content) {
                if (null == location || null == location.getData()) {
                    result.add(null);
                } else {
                    result.add(new URL("file",
                                       null,
                                       location.getData()));
                }
            }

        } catch (JAXBException
                 | IOException e) {
            throw new ResolutionException(e);
        }
        return result;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
