package gov.lbl.nest.catalog.mongo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;

import gov.lbl.nest.catalog.Criterium;
import gov.lbl.nest.catalog.rs.Entries;
import gov.lbl.nest.catalog.rs.EntryImpl;
import gov.lbl.nest.catalog.rs.criterium.MongoCriteriumVisitor;
import gov.lbl.nest.common.configure.CustomConfigPath;

/**
 * This class manages the documents container in a mongoDB
 * 
 * @author patton
 */
public class DocumentManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    private static final Comparator<EntryImpl> ENTRYIMPL_COMPARATOR = new Comparator<EntryImpl>() {

        @SuppressWarnings("unchecked")
        @Override
        public int compare(EntryImpl lhs,
                           EntryImpl rhs) {
            final List<? extends Comparable<?>> lefts = lhs.getComparables();
            final Iterator<? extends Comparable<?>> rights = (rhs.getComparables()).iterator();
            for (Comparable<?> left : lefts) {
                final Comparable<?> right = rights.next();
                if (null == left) {
                    if (null == right) {
                        return 0;
                    }
                    return -1;
                }
                if (null == right) {
                    return 1;
                }
                final int result = compareEntries(left,
                                                  right,
                                                  left.getClass());
                if (0 != result) {
                    return result;
                }
            }
            return 0;
        }
    };

    /**
     * The default path to the XML file containing the settings for this object.
     */
    private static final String DEFAULT_PATH = "catalog/mongodb.properties";

    /**
     * The name of the resource that may contain the path to an alternate default
     * configuration file for RMQ.
     */
    private static final String MONGO_CONFIG_RESOURCE = "mongo_config";

    /**
     * The {@link Logger} instance used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DocumentManager.class);

    private static final String MONGO_ADMINDB_PROPERTY = "gov.lbl.nest.catalog.mongo.admindb";

    private static final String MONGO_COLLECTION_PROPERTY = "gov.lbl.nest.catalog.mongo.collection";

    private static final String MONGO_DATABASE_PROPERTY = "gov.lbl.nest.catalog.mongo.database";

    private static final String MONGO_HOST_PROPERTY = "gov.lbl.nest.catalog.mongo.host";

    private static final String MONGO_PASSWORD_PROPERTY = "gov.lbl.nest.catalog.mongo.password";

    private static final String MONGO_PORT_PROPERTY = "gov.lbl.nest.catalog.mongo.port";

    private static final String MONGO_USER_PROPERTY = "gov.lbl.nest.catalog.mongo.user";

    /**
     * The name of the system property containing the user's home directory.
     */
    private static final String USER_HOME_PROPERTY = "user.home";

    // private static member data

    /**
     * True if the {@link #configFileName} value has been loaded.
     */
    private static final CustomConfigPath CONFIG_PATH = new CustomConfigPath(DocumentManager.class,
                                                                             MONGO_CONFIG_RESOURCE);

    // private instance member data

    @SuppressWarnings("unchecked")
    private static <T extends Comparable<T>> int compareEntries(Comparable<?> lhs,
                                                                Comparable<?> rhs,
                                                                Class<T> claszz) {
        return ((Comparable<T>) lhs).compareTo((T) rhs);
    }

    // constructors

    /**
     * Returns a new instance of a {@link MongoClient}.
     * 
     * @return a new instance of a {@link MongoClient}.
     * 
     * @throws IOException
     *             when the mongo properties can not be read.
     */
    private static MongoCollection<Document> getCollection() {
        final Path filePath;
        final Path configPath = CONFIG_PATH.getConfigPath();
        if (null == configPath) {
            final String home = System.getProperty(USER_HOME_PROPERTY);
            filePath = (new File(home,
                                 DEFAULT_PATH).getAbsoluteFile()).toPath();
        } else {
            filePath = configPath;
        }
        final Properties props = new Properties();
        try {
            props.load(new FileInputStream(filePath.toFile()));
            LOG.info("Read mongodb properties from \"" + filePath
                     + "\"");
            final Set<String> names = props.stringPropertyNames();
            if (names.isEmpty()) {
                LOG.warn("No mongo properties were specified, using defaults");
            } else {
                LOG.info("The following mongo properties were specified");
                for (String name : names) {
                    if (MONGO_PASSWORD_PROPERTY.equals(name)) {
                        LOG.info("  " + name
                                 + " = "
                                 + "<provided>");
                    } else {
                        LOG.info("  " + name
                                 + " = "
                                 + props.getProperty(name));
                    }
                }
            }
        } catch (IOException e) {
            LOG.warn("Mongodb properties file \"" + filePath
                     + "\" could not be successfully read, using defaults instead");
            props.clear();
        }
        final MongoCredential credential = MongoCredential.createCredential(props.getProperty(MONGO_USER_PROPERTY),
                                                                            props.getProperty(MONGO_ADMINDB_PROPERTY,
                                                                                              props.getProperty(MONGO_DATABASE_PROPERTY)),
                                                                            (props.getProperty(MONGO_PASSWORD_PROPERTY)).toCharArray());

        final ServerAddress serverAddress = new ServerAddress(props.getProperty(MONGO_HOST_PROPERTY,
                                                                                "localhost"),
                                                              Integer.parseInt(props.getProperty(MONGO_PORT_PROPERTY,
                                                                                                 "27017")));
        final MongoClientSettings.Builder settingsBuilder = MongoClientSettings.builder();
        settingsBuilder.credential(credential);
        settingsBuilder.applyToClusterSettings(builder -> builder.hosts(Arrays.asList(serverAddress)));
        settingsBuilder.readPreference(ReadPreference.primaryPreferred());
        final MongoClient client = MongoClients.create(settingsBuilder.build());

        LOG.info("Opened connection to MongoDB \"" + serverAddress
                 + "\"");
        final MongoDatabase database = client.getDatabase(props.getProperty(MONGO_DATABASE_PROPERTY));
        final MongoCollection<Document> collection = database.getCollection(props.getProperty(MONGO_COLLECTION_PROPERTY));
        return collection;
    }

    /**
     * @param criteria
     * @param alltags
     * @param tags
     * 
     * @return
     */
    // instance member method (alphabetic)

    /**
     * The Mongo collection instance used by this object.
     */
    private MongoCollection<Document> collection;

    // static member methods (alphabetic)

    /**
     * Creates an instance of this class.
     */
    public DocumentManager() {
        try {
            collection = getCollection();
        } catch (Exception e) {
            e.printStackTrace();
            collection = null;
            LOG.error("Can not open Mongodb Collection all Mongdb transactions will be ignored");
        }
    }

    /**
     * Returns an {@link Entries} instance containing all the entries that match
     * that meet specified {@link Criterium}.
     * 
     * @param criteria
     *            the {@link Criterium} that all elements of the returned
     *            {@link Entries} instance must match.
     * @param alltags
     *            true is all tags should be included in which case the contents of
     *            the tags parameter will be ignored.
     * @param tags
     *            the list of tags to include in the result.
     * @param ordering
     *            the sequence of tags that should be used for ordering the
     *            resulting list. (The tags enumerated here are not required to be
     *            present in the <code>tags</code> parameter.
     * 
     * @return t{@link Entries} instance containing all the entries that match that
     *         meet specified {@link Criterium}.
     */
    public Entries getEntries(final Criterium criteria,
                              final boolean alltags,
                              final List<String> tags,
                              final List<String> ordering) {
        if (null == collection) {
            LOG.error("Mongodb Collection is unavailable, ignoring request for entries");
            return null;
        }

        final MongoCriteriumVisitor visitor = new MongoCriteriumVisitor();
        criteria.build(visitor);
        final Bson compoundCriterium = visitor.getCompoundCriterium();
        LOG.info(compoundCriterium.toString());

        final List<EntryImpl> entries = new ArrayList<EntryImpl>();
        Consumer<Document> collectEntries = new Consumer<Document>() {
            @Override
            public void accept(final Document document) {
                entries.add(new EntryDocument(document,
                                              alltags,
                                              tags,
                                              ordering));
            }
        };
        final FindIterable<Document> iterable;
        if (false == alltags && null != tags) {
            Bson projection = Projections.include(tags);
            iterable = (collection.find(compoundCriterium)).projection(projection);
        } else {
            iterable = (collection.find(compoundCriterium));
        }
        iterable.forEach(collectEntries);

        if (null != ordering && !ordering.isEmpty()) {
            Collections.sort(entries,
                             ENTRYIMPL_COMPARATOR);
        }

        return new Entries(entries);
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
