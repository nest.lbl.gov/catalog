package gov.lbl.nest.catalog.mongo;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bson.Document;

import gov.lbl.nest.catalog.rs.EntryImpl;
import gov.lbl.nest.catalog.rs.Tags;
import gov.lbl.nest.common.xml.item.Item;

/**
 * This class is used to construct a suitable {@link EntryImpl} instance.
 * 
 * @author patton
 *
 */
public class EntryDocument extends
                           EntryImpl {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    private static List<? extends Comparable<?>> createComparables(final Document document,
                                                                   final List<String> ordering) {
        if (null == ordering || ordering.isEmpty()) {
            return null;
        }
        List<Comparable<?>> list = new ArrayList<Comparable<?>>();
        for (String item : ordering) {
            final Object object = document.get(item);
            if (null == object) {
                list.add(null);
            } else {
                list.add((Comparable<?>) object);
            }
        }
        return list;
    }

    private static Tags createTags(final Document document,
                                   final boolean alltags,
                                   final List<String> tags) {
        final Set<String> keys = document.keySet();
        final List<Item> tagsToReturn = new ArrayList<Item>();
        for (String key : keys) {
            if (!("identity".equals(key) || "_id".equals(key)
                  || "url".equals(key))) {
                if (alltags || tags.contains(key)) {
                    final Object object = document.get(key);
                    tagsToReturn.add(new Item(key,
                                              object));
                }
            }
        }
        return new Tags(tagsToReturn);
    }

    private static URL createURL(final Document document) {
        final String url = document.getString("url");
        if (null == url) {
            return null;
        }
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    /**
     * Creates an instance of this class.
     * 
     * @param document
     *            the {@link Document} instance from which to create this instance.
     * @param alltags
     *            true is all tag values should be returned.
     * @param tags
     *            the collection of tag value to return in this instance.
     * @param ordering
     *            TODO
     */
    public EntryDocument(final Document document,
                         final boolean alltags,
                         final List<String> tags,
                         List<String> ordering) {
        super(document.getString("identity"),
              createURL(document),
              createTags(document,
                         alltags,
                         tags),
              createComparables(document,
                                ordering));
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
