package gov.lbl.nest.catalog.mongo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.catalog.Catalog;
import gov.lbl.nest.catalog.CatalogDB;
import gov.lbl.nest.catalog.Criterium;
import gov.lbl.nest.catalog.Entry;
import gov.lbl.nest.catalog.ResolutionException;
import gov.lbl.nest.catalog.TagResolver;
import gov.lbl.nest.catalog.rs.Entries;
import gov.lbl.nest.common.xml.item.Item;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;

/**
 * This class implements the {@link Catalog} interface using an mongoDB backend.
 * 
 * @author patton
 *
 */
@Stateless
public class CatalogImpl implements
                         Catalog {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Map} instance, indexed by tag names, of {@link TagResolver}
     * instance to use for that name.
     */
    private static final Map<String, TagResolver> resolvers = TagResolver.getResolvers();

    // private static member data

    // private instance member data

    /**
     * The {@link DocumentManager} instance used by this object.
     */
    @Inject
    @CatalogDB
    private DocumentManager manager;

    // constructors

    // instance member method (alphabetic)

    @Override
    public boolean add(Entry entry) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Entries get(Criterium criterium,
                       boolean alltags,
                       List<String> tags,
                       List<String> ordering) throws ResolutionException {
        final Entries entries = manager.getEntries(criterium,
                                                   alltags,
                                                   tags,
                                                   ordering);
        for (Entry entry : entries.getContent()) {
            final List<Item> items = (entry.getTags()).getContent();
            /*
             * Note: `items` is an active array so any modifications will appear in the
             * entry without having to `set` a new value for the Tags content.
             */
            resolveTags(items);
        }
        return entries;
    }

    private void resolveTags(final List<Item> items) {
        final List<Item> modifiedTags = new ArrayList<Item>();
        final List<Item> derivedTags = new ArrayList<Item>();
        final Iterator<Item> iterator = items.iterator();
        while (iterator.hasNext()) {
            final Item item = iterator.next();
            final String name = item.getName();
            final TagResolver resolver = resolvers.get(name);
            if (resolver != null) {
                final List<Item> resolvedTags = resolver.resolveTag(item);
                if (null != resolvedTags && !resolvedTags.isEmpty()) {
                    final Item first = resolvedTags.get(0);
                    if (name.equals(first.getName())) {
                        modifiedTags.add(first);
                        resolvedTags.remove(0);
                        items.remove(item);
                    }
                    derivedTags.addAll(resolvedTags);
                }
            }
        }
        items.addAll(modifiedTags);
        if (derivedTags.isEmpty()) {
            return;
        }
        resolveTags(derivedTags);
        items.addAll(derivedTags);
    }

    @Override
    public Entry update(Entry entry) {
        // TODO Auto-generated method stub
        return null;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
