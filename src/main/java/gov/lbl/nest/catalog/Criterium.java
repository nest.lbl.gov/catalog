package gov.lbl.nest.catalog;

import gov.lbl.nest.catalog.rs.criterium.CriteriumVisitor;
import gov.lbl.nest.catalog.rs.criterium.Equality;
import gov.lbl.nest.catalog.rs.criterium.Greater;
import gov.lbl.nest.catalog.rs.criterium.GreaterOrEqual;
import gov.lbl.nest.catalog.rs.criterium.Less;
import gov.lbl.nest.catalog.rs.criterium.LessOrEqual;
import gov.lbl.nest.catalog.rs.criterium.LogicalAnd;
import gov.lbl.nest.catalog.rs.criterium.LogicalNot;
import gov.lbl.nest.catalog.rs.criterium.LogicalOr;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This interface is used to select zero, one or more {@link Entry} instances
 * from the catalog.
 * 
 * @author patton
 */
@XmlType
@XmlSeeAlso(value = { Equality.class,
                      Greater.class,
                      GreaterOrEqual.class,
                      Less.class,
                      LessOrEqual.class,
                      LogicalOr.class,
                      LogicalAnd.class,
                      LogicalNot.class })

public abstract class Criterium {

    /**
     * accepts a {@link CriteriumVisitor} instance in order to build a complete
     * criteria.
     * 
     * @param visitor
     *            the {@link CriteriumVisitor} instance visiting this instance.
     */
    public abstract void build(CriteriumVisitor visitor);
}
