package gov.lbl.nest.catalog;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.zip.Checksum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.xml.item.Item;

/**
 * This class creates {@link TagResolver} instances.
 * 
 * @author patton
 */

public abstract class TagResolver {

    /**
     * The {@link ServiceLoader} instance that will load the {@link ChecksumElement}
     * implementation.
     */
    private final static ServiceLoader<TagResolver> LOADER = ServiceLoader.load(TagResolver.class,
                                                                                TagResolver.class.getClassLoader());

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(TagResolver.class);

    /**
     * Returns an {@link Map} instance, indexed by tag names, of {@link TagResolver}
     * instance to use for that name.
     * 
     * @return the {@link Checksum} instance to be used to calculate the checksum.
     */
    public static Map<String, TagResolver> getResolvers() {
        final Map<String, TagResolver> result = new HashMap<String, TagResolver>();
        for (TagResolver resolver : LOADER) {
            final Collection<String> names = resolver.getTags();
            if (null != names && !names.isEmpty()) {
                for (String name : names) {
                    if (result.containsKey(name)) {
                        LOG.warn("Tag name \"" + name
                                 + "\" has multiple TagResolvers specified");
                    } else {
                        result.put(name,
                                   resolver);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the collection of tag names that this object can resolve.
     * 
     * @return the collection of tag names that this object can resolve.
     */
    protected abstract Collection<String> getTags();

    /**
     * Returns a collection of modified and new tags that result from the
     * resolution. If the original tag has been modified the new version of the tag
     * must be the first element in the returned collection.
     * 
     * @param tag
     *            the Tag to be resolved.
     * 
     * @return the collection of modified and new tags that result from the
     *         resolution.
     */
    public abstract List<Item> resolveTag(Item tag);

}
