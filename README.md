[Catalog](https://gitlab.com/nest.lbl.gov/catalog) provides a JEE/RESTful implementation of a keyword searchable catalog.

To deploy this basic application you will need a JEE Application Server. The discussion here will use the standalone [Wildfly](http://wildfly.org/) implementation of this, so if you are using a different implementation you may need to alter various steps.
